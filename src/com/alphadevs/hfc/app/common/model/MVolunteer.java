package com.alphadevs.hfc.app.common.model;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MVolunteer implements Parcelable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("location")
	private String location;
	@JsonProperty("description")
	private String description;
	@JsonProperty("followingSince")
	private Date followingSince;

	public MVolunteer() {
		// Default constructon for json and other usages. donot remove
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFollowingSince(Date followingSince) {
		this.followingSince = followingSince;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public String getDescription() {
		return description;
	}

	public Date getFollowingSince() {
		return followingSince;
	}



	/**
	 * consructor used while deserializing the parcel
	 * 
	 * @param in
	 *            : parcel object
	 */
	private MVolunteer(Parcel in) {
		id = in.readString();
		name = in.readString();
		location = in.readString();
		description = in.readString();
		followingSince = (Date) in.readSerializable();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(location);
		dest.writeString(description);
		dest.writeSerializable(followingSince);
	}
	public Parcelable.Creator<MVolunteer> CREATOR = new Creator<MVolunteer>() {

		@Override
		public MVolunteer[] newArray(int size) {
			return new MVolunteer[size];
		}

		@Override
		public MVolunteer createFromParcel(Parcel in) {
			return new MVolunteer(in);
		}
	};
}
