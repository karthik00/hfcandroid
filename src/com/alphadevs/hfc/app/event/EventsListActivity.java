package com.alphadevs.hfc.app.event;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.util.HFCConstants;
import com.hfc.app.R;

public class EventsListActivity extends H4CFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events_list);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_events_list, menu);
		return true;
	}

	public void onEventProfileButtonClick(View view) {
		Intent eventProfileIntent = new Intent(this, EventProfileActivity.class);
		// to do get the eventkey details object and put it as an private
		eventProfileIntent.putExtra(HFCConstants.EVENT_KEY, "evnt01");
		startActivity(eventProfileIntent);
	}
}
