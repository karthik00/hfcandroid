package com.alphadevs.hfc.app.volunteer;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.alphadevs.hfc.app.common.model.MNgo;
import com.alphadevs.hfc.app.ngo.NgoProfileActivity;
import com.alphadevs.hfc.app.services.Callback;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;

public class VolunteerFollowingNgos extends Fragment implements VolunteerProfileActivityEventsListener {

	private ArrayList<MNgo> volunteerFollowinfNgoList;

	public VolunteerFollowingNgos() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		((VolunteerProfilePagerFragEventsLiserner) getActivity()).onFragCreated(0);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			volunteerFollowinfNgoList = savedInstanceState.getParcelableArrayList("VOLUNTEER_FOLLOWING_NGO_LIST");
		}
		return inflater.inflate(R.layout.volunteer_following_ngos_fragment, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		initNgoGridItems();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	private class VolunteerFollowingNgosListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return volunteerFollowinfNgoList == null ? 0 : volunteerFollowinfNgoList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View ngoGridItem = layoutInflater.inflate(R.layout.ngo_grid_item, parent, false);
			TextView ngoGridItemNgoNameTV = (TextView) ngoGridItem.findViewById(R.id.ngo_grid_item_ngo_name);
			ngoGridItemNgoNameTV.setText(volunteerFollowinfNgoList.get(position).getName());
			TextView ngoGridItemNgoLocTV = (TextView) ngoGridItem.findViewById(R.id.ngo_grid_item_ngo_loc);
			ngoGridItemNgoLocTV.setText(volunteerFollowinfNgoList.get(position).getLocationsAsString());
			return ngoGridItem;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int postion, long id) {
			Intent ngoProfileIntent = new Intent(getActivity(), NgoProfileActivity.class);
			//ngoProfileIntent.putExtra("NGO_ID", volunteerFollowinfNgoList.get(postion).getId());
			//ngoProfileIntent.putExtra("NGO", volunteerFollowinfNgoList.get(postion));
			getActivity().startActivity(ngoProfileIntent);
		}
	}

	@Override
	public void onVolunteerProfileHelperServiceConnected(VolunteerProfileHelperService volunteerProfileHelperService) {
		volunteerProfileHelperService.getVolunteerFollowingNgos(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == VolunteerProfileHelperService.FETCH_VOLUNTEER_ATTENDING_EVENTS_SUCCESS) {
					String volunteerFollowingNgosJsonString = message.getData().getString("VOLUNTEER_FOLLOWING_NGOS_JSON");
					try {
						volunteerFollowinfNgoList = (new ObjectMapper()).readValue(volunteerFollowingNgosJsonString, new TypeReference<ArrayList<MNgo>>() {});
						initNgoGridItems();
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void initNgoGridItems() {
		View volunteerFollowingNgosFragView = this.getView();
		if (volunteerFollowingNgosFragView != null) {
			GridView ngoGridItemsView = (GridView) volunteerFollowingNgosFragView.findViewById(R.id.volunteer_following_grid);
			ngoGridItemsView.setAdapter(new VolunteerFollowingNgosListAdapter());
			VolunteerFollowingNgosListAdapter followingNgosListAdapter = (VolunteerFollowingNgosListAdapter) ngoGridItemsView.getAdapter();
			ngoGridItemsView.setOnItemClickListener(followingNgosListAdapter);
			followingNgosListAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("VOLUNTEER_FOLLOWING_NGO_LIST", volunteerFollowinfNgoList);
	}
}
