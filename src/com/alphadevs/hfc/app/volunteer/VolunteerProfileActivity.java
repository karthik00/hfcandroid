package com.alphadevs.hfc.app.volunteer;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.volunteer.VolunteerProfileHelperService.VolunteerProfileServiceBinder;
import com.hfc.app.R;

public class VolunteerProfileActivity extends H4CFragmentActivity implements VolunteerProfilePagerFragEventsLiserner {

	private ViewPager mVolunteerProfileViewPager;
	private VolunteerProfileHelperService volunteerProfileHelperService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		setContentView(R.layout.activity_volunteer_profile);
		/*
		 * String stringExtra = intent.getStringExtra("ID"); MVolunteer
		 * volunteer = intent.getParcelableExtra("MVOLUNTEER"); if (volunteer !=
		 * null) { ((TextView)
		 * findViewById(R.id.volunteer_profile_activity_title
		 * )).setText(volunteer.getName()); }
		 */
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
		getActionBar().setDisplayShowHomeEnabled(true);
		View homeIcon = findViewById(android.R.id.home);
	}

	private void initPagerFrags() {
		mVolunteerProfileViewPager = (ViewPager) findViewById(R.id.volunteerProfileHomeHSwipePager);
		mVolunteerProfileViewPager.setAdapter(new VolunteerProfielSectionAdapter(getSupportFragmentManager()));
		VolunteerProfielSectionAdapter adapter = (VolunteerProfielSectionAdapter) mVolunteerProfileViewPager.getAdapter();
		mVolunteerProfileViewPager.setCurrentItem(1);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Intent volunteerHelperServiceIntent = new Intent(this, VolunteerProfileHelperService.class);
		bindService(volunteerHelperServiceIntent, volunteerProfileHelperServiceCon, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(volunteerProfileHelperServiceCon);
		super.onStop();
	}

	@Override
	public void onFragCreated(int position) {
		VolunteerProfileActivityEventsListener activityEventListenerFrag =
				((VolunteerProfielSectionAdapter) mVolunteerProfileViewPager.getAdapter()).getVolunteerProfileActivityEventsListener(position);
		activityEventListenerFrag.onVolunteerProfileHelperServiceConnected(volunteerProfileHelperService);
	}
	private ServiceConnection volunteerProfileHelperServiceCon = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			volunteerProfileHelperService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			volunteerProfileHelperService = ((VolunteerProfileServiceBinder) service).getService();
			initPagerFrags();
		}
	};
	private class VolunteerProfielSectionAdapter extends FragmentPagerAdapter {

		private VolunteerFollowingNgos volunteerFollowingNgos;
		private AboutVolunteerFragment aboutVolunteerFragment;
		private VolunteerAttendingEvents volunteerAttendingEvents;

		public VolunteerProfielSectionAdapter(FragmentManager fm) {
			super(fm);
			volunteerFollowingNgos = new VolunteerFollowingNgos();
			aboutVolunteerFragment = new AboutVolunteerFragment();
			volunteerAttendingEvents = new VolunteerAttendingEvents();
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			switch (position) {
				case 0:
					return volunteerFollowingNgos;
				case 1:
					return aboutVolunteerFragment;
				case 2:
					return volunteerAttendingEvents;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "NGOs";
				case 1:
					return "About";
				case 2:
					return "AttendingEvents";
			}
			return null;
		}

		public VolunteerProfileActivityEventsListener getVolunteerProfileActivityEventsListener(int position) {
			return (VolunteerProfileActivityEventsListener) this.getItem(position);
		}
	}
	private class AboutVolunteerFragment extends Fragment implements VolunteerProfileActivityEventsListener {

		public AboutVolunteerFragment() {
			super();
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			super.onCreate(savedInstanceState);
			((VolunteerProfilePagerFragEventsLiserner) getActivity()).onFragCreated(1);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.volunteer_about_volunteer, container, false);
		}

		@Override
		public void onVolunteerProfileHelperServiceConnected(VolunteerProfileHelperService volunteerProfileHelperService) {
			// TODO Auto-generated method stub
		}
	}
}
