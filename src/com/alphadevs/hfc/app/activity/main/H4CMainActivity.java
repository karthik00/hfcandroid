package com.alphadevs.hfc.app.activity.main;

import java.util.Arrays;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.alphadevs.hfc.app.services.AuthenticationService;
import com.alphadevs.hfc.app.services.AuthenticationService.AuthenticationBinder;
import com.alphadevs.hfc.app.services.AuthenticationService.AuthenticationResponse;
import com.alphadevs.hfc.app.services.Callback;
import com.alphadevs.hfc.app.user.UserProfile;
import com.alphadevs.hfc.app.util.HFCAndroidSessionManager;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.hfc.app.R;
import com.loopj.android.http.RequestParams;

public class H4CMainActivity extends H4CFragmentActivity {

	private EditText unameView;
	private EditText passwordView;
	private Button loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (HFCAndroidSessionManager.isSessionActive()) {
			setContentView(R.layout.searchlayout_ngo_events);
		} else setContentView(R.layout.activity_h4c_main);
		uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		unameView = (EditText) findViewById(R.id.editText1);
		passwordView = (EditText) findViewById(R.id.editText2);
		loginButton = (Button) findViewById(R.id.loginButton);
		loginButton.setEnabled(false);
	}

	@Override
	protected void onStart() {
		super.onStart();
		Intent intent = new Intent(this, AuthenticationService.class);
		bindService(intent, mAuthenticationServiceConnection, Context.BIND_AUTO_CREATE);
	}
	private AuthenticationService authService;
	private ServiceConnection mAuthenticationServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			authService = null;
			loginButton.setEnabled(false);
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			authService = ((AuthenticationBinder) binder).getService();
			loginButton.setEnabled(true);
		}
	};

	public void onFBLoginListener(View view) {
		Session session = Session.getActiveSession();
		if (session == null) {
			if (session == null) {
				session = new Session(this);
			}
			Session.setActiveSession(session);
			if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
				session.openForRead(new Session.OpenRequest(this).setPermissions(Arrays.asList("email")).setCallback(statusCallback));
			}
		}
		Session.openActiveSession(this, true, statusCallback);
	}
	private Session.StatusCallback statusCallback = new Session.StatusCallback() {

		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (state.isOpened()) {
				String accessToken = session.getAccessToken();
				if (accessToken != null || !"".equals(accessToken)) {
					RequestParams params = new RequestParams();
					params.put("token", accessToken);
					final ProgressDialog loginProgress = new ProgressDialog(H4CMainActivity.this);
					authService.loginWithFB(new Callback() {

						@Override
						public void call(Message message) {
							if (message.what == AuthenticationResponse.SUCCESS) {
								Intent intent = new Intent(H4CMainActivity.this, UserProfile.class);
								startActivity(intent);
							} else {
								// TODO
							}
							loginProgress.dismiss();
						}
					}, params);
					loginProgress.show();
				}
			}
		}
	};

	public void onloginListener(View view) {
		RequestParams params = new RequestParams();
		params.put("email", unameView.getText().toString());
		params.put("password", passwordView.getText().toString());
		final ProgressDialog loginProgress = new ProgressDialog(this);
		authService.login(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == AuthenticationResponse.SUCCESS) {
					Intent intent = new Intent(H4CMainActivity.this, UserProfile.class);
					startActivity(intent);
				} else {
					// TODO
				}
				loginProgress.dismiss();
			}
		}, params);
		loginProgress.show();
	}
	private UiLifecycleHelper uiHelper;

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		unbindService(mAuthenticationServiceConnection);
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
}
