package com.alphadevs.hfc.app.user;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.hfc.app.R;

public class UserProfile extends H4CFragmentActivity implements ActionBar.TabListener {

	private ViewPager muserProfileViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.userprofile);
		muserProfileViewPager = (ViewPager) findViewById(R.id.user_profile_pager);
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getActionBar().setDisplayShowHomeEnabled(true);
		View homeIcon = findViewById(android.R.id.home);
		((View) homeIcon.getParent()).setVisibility(View.GONE);
		getActionBar().setCustomView(R.layout.actionbar_title);
		muserProfileViewPager.setAdapter(new UserProfielSectionAdapter(getSupportFragmentManager()));
		UserProfielSectionAdapter adapter = (UserProfielSectionAdapter) muserProfileViewPager.getAdapter();
		muserProfileViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				getActionBar().setSelectedNavigationItem(position);
			}
		});
		for (int i = 0; i < adapter.getCount(); i++) {
			ActionBar actionBar = getActionBar();
			actionBar.addTab(actionBar.newTab().setIcon(adapter.getIconId(i)).setTabListener(this));
		}
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {}
	public static class userCalendarFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			return inflater.inflate(R.layout.following_volunteers, null);
		}
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		muserProfileViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {}
	private class UserProfielSectionAdapter extends FragmentPagerAdapter {

		public UserProfielSectionAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			switch (position) {
				case 0:
					return new UserProfileHomeFragment();
				case 1:
					return new UserProfileHomeFragment();
				case 2:
					return new UserProfileHomeFragment();
				case 3:
					return new UserProfileHomeFragment();
			}
			return null;
		}

		@Override
		public int getCount() {
			return 4;
		}

		public int getIconId(int position) {
			switch (position) {
				case 0:
					return R.drawable.ic_menu_user_profile_home;
				case 1:
					return R.drawable.ic_action_user_social_connections;
				case 2:
					return R.drawable.ic_action_user_contact;
				case 3:
					return R.drawable.ic_action_user_calender;
			}
			return 0;
		}
	}

	public void showUserProfile(View view) {
		Intent intent = new Intent(this, UserConnections.class);
		startActivity(intent);
	}
}
