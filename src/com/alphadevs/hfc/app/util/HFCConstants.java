package com.alphadevs.hfc.app.util;

public interface HFCConstants {

	public static final String EVENT_KEY = "EVENT_KEY";
	public static final String VOLUNTEER_KEY = "Volunteer_Key";
	public static final String FollwinEventsKey = "Following_Events_Key";
	public static final String VOLUNTEER_LIST_KEY = "VOLUNTEER_LIST_KEY";
	public static final String FOLLOWING_VOLUNTEERS_KEY = "FOLLOWING_VOLUNTEERS_KEY";
	public static final String FOLLOWING_NGO_KEY = "FOLLOWING_NGO_KEY";
	public static final String LOGIN_SUCCESS_STATUS = "SUCCESS";
	public static final String LOGIN_FAILED_STATUS = "FAILED";
	public static final int USER_CONNECTION_TAB_TITLE_COUNT = 3;
	public static final CharSequence USER_FOLLOWING_NGOS_TAB_TITLE = "Followinf NGO's";
	public static final CharSequence USER_FOLLOWING_VOLUNTEERS_TAB_TITLE = "Following Volunteers";
	public static final CharSequence USER_ATTENDING_EVENTS_TAB_TITLE = "Attending Events";
	public static final int USER_FOLLOWING_VOLUNTEERS_TAB_INDEX = 0;
	public static final int USER_FOLLOWING_NGOS_TAB_INDEX = 1;
	public static final int USER_ATTENDING_EVENTS_TAB_INDEX = 2;
	public static final int UPDATE_NOT_STARTED = 0;
	public static final int UPDATE_IN_PROGRESS = 1;
	public static final int UPDATE_COMPLETE = 2;
	public static final int UPDATE_FAILED = 3;
}
