package com.alphadevs.hfc.app.ngo;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.alphadevs.hfc.app.common.model.MEvent;
import com.alphadevs.hfc.app.event.EventProfileActivity;
import com.alphadevs.hfc.app.services.Callback;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;

public class NgoProfileEventListFragment extends Fragment implements NgoProfileActivityEventsLinstner {

	private NgoHelperService ngoHelperService;
	private ArrayList<MEvent> ngoOrganizedEvents;
	private GridView eventsGrid;

	public NgoProfileEventListFragment() {
		super();
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((NgoPagerFragEventsLiserner) getActivity()).onPagerFragViewCreated(2);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			ngoOrganizedEvents = savedInstanceState.getParcelableArrayList("NGO_EVENTS_LIST");
		}
		setGridItems();
		return inflater.inflate(R.layout.ngo_events_list_frag, container, false);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		eventsGrid = (GridView) this.getView().findViewById(R.id.following_events_grid);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		setGridItems();
	}
	private class AttendingEventsListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return ngoOrganizedEvents == null ? 0 : ngoOrganizedEvents.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(R.layout.event_grid_item, parent, false);
			MEvent event = ngoOrganizedEvents.get(position);
			((TextView) view.findViewById(R.id.event_grid_event_name_tv)).setText(event.getName());
			TextView eventDateTv = (TextView) view.findViewById(R.id.event_grid_event_date_tv);
			eventDateTv.setText(DateFormat.format("MMM dd, yyyy h:mmaa", event.getEventDate()));
			TextView eventLocTv = (TextView) view.findViewById(R.id.event_grid_event_location_tv);
			eventLocTv.setText(event.getLocationsAsString());
			return view;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			Intent intent = new Intent(getActivity(), EventProfileActivity.class);
			getActivity().startActivity(intent);
		}
	}

	@Override
	public void onNgoHelperServiceConnected(NgoHelperService helperService) {
		// TODO Auto-generated method stub
		this.ngoHelperService = helperService;
		ngoHelperService.getNgoEvents(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what==NgoHelperService.FETCH_NGO_EVETS_SUCCESS) {
					String ngoEvetsJson = message.getData().getString("GET_NGO_EVENTS_RESPONSE_JSON");
					try {
						ngoOrganizedEvents = (new ObjectMapper()).readValue(ngoEvetsJson, new TypeReference<ArrayList<MEvent>>() {});
						setGridItems();
					} catch (JsonParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});
	}

	private void setGridItems() {
		View fragmentView = this.getView();
		if (fragmentView != null) {
			eventsGrid = (GridView) fragmentView.findViewById(R.id.following_events_grid);
			AttendingEventsListAdapter attendingEventsListAdapter = new AttendingEventsListAdapter();
			eventsGrid.setAdapter(attendingEventsListAdapter);
			eventsGrid.setOnItemClickListener(attendingEventsListAdapter);
			attendingEventsListAdapter.notifyDataSetChanged();
		}
	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("NGO_EVENTS_LIST", ngoOrganizedEvents);
	}
}
