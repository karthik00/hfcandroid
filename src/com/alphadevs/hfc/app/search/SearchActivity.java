package com.alphadevs.hfc.app.search;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.common.model.FieldOfActions;
import com.alphadevs.hfc.app.common.model.SearchRequirements;
import com.alphadevs.hfc.app.common.model.SearchResult;
import com.alphadevs.hfc.app.core.db.DatabaseHelper;
import com.alphadevs.hfc.app.event.EventProfileActivity;
import com.alphadevs.hfc.app.ngo.NgoProfileActivity;
import com.alphadevs.hfc.app.serviceconsumers.DatabaseServiceConsumer;
import com.alphadevs.hfc.app.serviceconsumers.HfcWSClientConsumer;
import com.alphadevs.hfc.app.services.Callback;
import com.alphadevs.hfc.app.services.DatabaseService;
import com.alphadevs.hfc.app.services.HFCServerCommunicationServices;
import com.alphadevs.hfc.app.services.SearchResultService;
import com.alphadevs.hfc.app.services.SearchResultService.SearchResponse;
import com.alphadevs.hfc.app.services.SearchResultService.SearchRestultBinder;
import com.alphadevs.hfc.app.util.HFCConstants;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;
import com.j256.ormlite.dao.Dao;

public class SearchActivity extends H4CFragmentActivity implements OnClickEventListener, HfcWSClientConsumer, DatabaseServiceConsumer, SearchFragEvetsListener, SearchResultFragEventListener {

	protected static final int NGO = 0;
	protected static final int EVENT = 1;
	public static int CLICKED_NGO = 2;
	public static int CLICKED_EVENTS = 3;
	public static String SEARCH_FIELDS_TAG = "search_fields";
	public static String SEARCH_RESULTS_TAG = "search_results";
	private HFCServerCommunicationServices communicationService;
	// private SearchFragment searchFrag;
	private DatabaseHelper dbHelper;
	private ProgressDialog mProgressDiag;
	private SearchResultService searchService;
	private SearchServiceCon searchServiceCon;
	private SearchResultBaseAdapter mAdapter;
	private FragmentManager fragManager;
	private FragmentTransaction fragTrans;
	private List<SearchResult> searchResults;
	private class SearchServiceCon implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			searchService = ((SearchRestultBinder) service).getService();
			DatabaseService.bindService(SearchActivity.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			searchService = null;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(android.R.style.Theme_Translucent);
		setContentView(R.layout.home);
		super.onCreate(savedInstanceState);
		mProgressDiag = new ProgressDialog(this);
		mProgressDiag.show();
	}

	@Override
	protected void onStart() {
		super.onStart();
		searchServiceCon = new SearchServiceCon();
		Intent intent = new Intent(this, SearchResultService.class);
		bindService(intent, searchServiceCon, Context.BIND_AUTO_CREATE);
	}

	private void setFragment() {
		FragmentManager fragManager = getSupportFragmentManager();
		FragmentTransaction fragTrans = fragManager.beginTransaction();
		fragTrans.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_in_right);
		if (fragManager.findFragmentByTag(SEARCH_FIELDS_TAG) == null) fragTrans.add(R.id.fragmentContainer, new SearchFragment(), SearchActivity.SEARCH_FIELDS_TAG);
		fragTrans.commit();
		mProgressDiag.dismiss();
	}

	@Override
	protected void onStop() {
		unbindService(searchServiceCon);
		if (dbHelper != null) DatabaseService.unbindService(SearchActivity.this);
		if (communicationService != null) HFCServerCommunicationServices.unbindService(this);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		SearchFragment searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag(SEARCH_FIELDS_TAG);
		if (!searchFragment.onBackPressed()) super.onBackPressed();
	}

	@Override
	public void onButtonClick(int clickedWhat) {
		final ProgressDialog displaySearchResultProgess = new ProgressDialog(this);
		displaySearchResultProgess.show();
		fragManager = getSupportFragmentManager();
		fragTrans = fragManager.beginTransaction();
		Fragment searchFieldsFragment = fragManager.findFragmentByTag(SEARCH_FIELDS_TAG);
		View searchFieldsFragView = searchFieldsFragment.getView();
		MultiAutoCompleteTextView searchRequirements = (MultiAutoCompleteTextView) searchFieldsFragView.findViewById(R.id.search_requirements);
		MultiAutoCompleteTextView searchfieldOfActionsView = (MultiAutoCompleteTextView) searchFieldsFragView.findViewById(R.id.search_field_of_actions);
		String requirementsString = searchRequirements.getText().toString();
		System.out.println("::: requirementsString ::::" + requirementsString);
		String requirement[] = null;
		if (requirementsString != null) {
			requirement = requirementsString.split(",");
		}
		String fieldOfActionsString = searchfieldOfActionsView.getText().toString();
		System.out.println("::::fieldOfActionsString  ::::" + fieldOfActionsString);
		String fieldOfActions[] = null;
		if (fieldOfActionsString != null) fieldOfActions = fieldOfActionsString.split(",");
		if (clickedWhat == SearchActivity.CLICKED_NGO) {
			searchService.getNgoSearchResult(new Callback() {

				@Override
				public void call(Message message) {
					if (message.what == SearchResponse.SUCCESS) {
						String result = message.getData().getString("SearchResult");
						try {
							searchResults = (new ObjectMapper()).readValue(result, new TypeReference<ArrayList<SearchResult>>() {});
							System.out.println(searchResults);
						} catch (JsonParseException e) {
							e.printStackTrace();
						} catch (JsonMappingException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						mAdapter = new SearchResultBaseAdapter();
						setSearchResultFor(SearchActivity.NGO);
						displaySearchResultProgess.dismiss();
					}
				}
			}, new SearchQuery(requirement, fieldOfActions));
		} else if (clickedWhat == SearchActivity.CLICKED_EVENTS) {
			// TODO event method in service
			/*
			 * searchService.getEventSearchResult(new Callback() {
			 * 
			 * @Override public void call(Message message) {
			 * 
			 * } }, new SearchQuery(requirement,fieldOfActions));
			 */
			searchService.getNgoSearchResult(new Callback() {

				@Override
				public void call(Message message) {
					if (message.what == SearchResponse.SUCCESS) {
						String result = message.getData().getString("SearchResult");
						try {
							searchResults = (new ObjectMapper()).readValue(result, new TypeReference<ArrayList<SearchResult>>() {});
							System.out.println(searchResults);
						} catch (JsonParseException e) {
							e.printStackTrace();
						} catch (JsonMappingException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						mAdapter = new SearchResultBaseAdapter();
						setSearchResultFor(SearchActivity.NGO);
						displaySearchResultProgess.dismiss();
					}
				}
			}, new SearchQuery(requirement, fieldOfActions));
		}
	}

	private void setSearchResultFor(int what) {
		if (fragManager.findFragmentByTag(SEARCH_RESULTS_TAG) != null) fragTrans.remove(fragManager.findFragmentByTag(SEARCH_RESULTS_TAG));
		fragTrans.setCustomAnimations(R.animator.slide_in_left, R.animator.slide_in_right);
		SearchResultsFragment searchResultsFrag = new SearchResultsFragment();
		fragTrans.add(R.id.fragmentContainer, searchResultsFrag, SearchActivity.SEARCH_RESULTS_TAG);
		fragTrans.commit();
	}

	@Override
	public void onSearchResultFragAttched() {
		ListView mListview = (ListView) getSupportFragmentManager().findFragmentByTag(SEARCH_RESULTS_TAG).getView().findViewById(R.id.search_results_list);
		mListview.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		final Button mNgoButton = (Button) getSupportFragmentManager().findFragmentByTag(SEARCH_FIELDS_TAG).getView().findViewById(R.id.search_button_searchNgo);
		mListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
				Intent intent = null;
				if (!mNgoButton.isEnabled()) {
					intent = new Intent(SearchActivity.this, NgoProfileActivity.class);
					intent.putExtra("NGO_ID", searchResults.get(position).getId());
					intent.putExtra("NGO", searchResults.get(position));
				} else {
					intent = new Intent(SearchActivity.this, EventProfileActivity.class);
					intent.putExtra("EVENT_ID", searchResults.get(position).getId());
					intent.putExtra("EVENT", searchResults.get(position));
				}
				SearchActivity.this.startActivity(intent);
			}
		});
	}

	@Override
	public void onServiceConnected(HFCServerCommunicationServices hfcServerCommunicationServices) {
		this.communicationService = hfcServerCommunicationServices;
		if (HFCServerCommunicationServices.updateMetaDataStatus == HFCConstants.UPDATE_NOT_STARTED) {
			communicationService.updateAppMetaData(dbHelper, new Handler() {

				@Override
				public void handleMessage(Message msg) {
					super.handleMessage(msg);
				}
			});
		}
		if (HFCServerCommunicationServices.updateMetaDataStatus == HFCConstants.UPDATE_IN_PROGRESS) {
			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					// TODO think of some alternate call back for this
					while (HFCServerCommunicationServices.updateMetaDataStatus == HFCConstants.UPDATE_IN_PROGRESS) {
					}
					return null;
				}

				@Override
				protected void onPreExecute() {
					super.onPreExecute();
				}

				protected void onPostExecute(Void result) {
					setFragment();
				};
			}.execute();
		} else {
			setFragment();
		}
	}

	@Override
	public void onServiceDisconnected() {
		this.communicationService = null;
	}

	@Override
	public void onDBServiceDisconnected() {
		this.dbHelper = null;
	}

	@Override
	public void onDBServiceConnected(DatabaseHelper dbHelpher) {
		this.dbHelper = dbHelpher;
		HFCServerCommunicationServices.bindService(this);
	}
	private static final String[] COUNTRIES = new String[] { "Belgium", "France", "Italy", "Germany", "Spain" };

	@Override
	public void onSearchFragStarted() {
		// TODO Auto-generated method stub
		FragmentManager fragmentManager = getSupportFragmentManager();
		Fragment searchFrag = (SearchFragment) fragmentManager.findFragmentByTag(SearchActivity.SEARCH_FIELDS_TAG);
		View searchFragView = searchFrag.getView();
		MultiAutoCompleteTextView mMultiTextViewSearchReqs = (MultiAutoCompleteTextView) searchFragView.findViewById(R.id.search_requirements);
		MultiAutoCompleteTextView mMultiTextViewFieldOfActions = (MultiAutoCompleteTextView) searchFragView.findViewById(R.id.search_field_of_actions);
		Spinner mSpinnerLocation = (Spinner) searchFragView.findViewById(R.id.search_locations_spinner);
		mMultiTextViewSearchReqs.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getSearchRequirementsAsArray()));
		mMultiTextViewFieldOfActions.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getFieldsOfActionAsArray()));
		mSpinnerLocation.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, COUNTRIES));
	}

	private String[] getFieldsOfActionAsArray() {
		Dao<FieldOfActions, Integer> dao = dbHelper.getDao(FieldOfActions.class);
		try {
			List<FieldOfActions> fieldsOfAction = dao.queryForAll();
			ArrayList<String> fieldsOfActionNameList = new ArrayList<String>();
			for (Iterator<FieldOfActions> iterator = fieldsOfAction.iterator(); iterator.hasNext();) {
				FieldOfActions fieldOfActions = (FieldOfActions) iterator.next();
				fieldsOfActionNameList.add(fieldOfActions.getName());
			}
			return fieldsOfActionNameList.toArray(new String[] {});
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String[] getSearchRequirementsAsArray() {
		Dao<SearchRequirements, Integer> dao = dbHelper.getDao(SearchRequirements.class);
		try {
			List<SearchRequirements> searchrequirements = dao.queryForAll();
			List<String> searchRequiremntsNameList = new ArrayList<String>();
			for (Iterator<SearchRequirements> iterator = searchrequirements.iterator(); iterator.hasNext();) {
				SearchRequirements searchRequirement = (SearchRequirements) iterator.next();
				searchRequiremntsNameList.add(searchRequirement.getName());
			}
			return searchRequiremntsNameList.toArray(new String[] {});
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	private class SearchResultBaseAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return searchResults == null ? 0 : searchResults.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = SearchActivity.this.getLayoutInflater();
			View view = layoutInflater.inflate(R.layout.search_result_item_view, parent, false);
			TextView searchResultTitleTV = (TextView) view.findViewById(R.id.searchResultTitle);
			searchResultTitleTV.setText(searchResults.get(position).getName());
			TextView searchResultDescTV = (TextView) view.findViewById(R.id.searchResultDescription);
			searchResultDescTV.setText(searchResults.get(position).getDescription());
			return view;
		}
	}
}
