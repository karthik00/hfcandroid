package com.alphadevs.hfc.app.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;

import com.alphadevs.hfc.app.core.db.DatabaseHelper;
import com.alphadevs.hfc.app.serviceconsumers.DatabaseServiceConsumer;

public class DatabaseService extends Service {

	private DatabaseHelper dbHelper;
	private final IBinder mBinder = new LocalBinder();
	private static DBServiceConnection dbServiceCon;
	public class LocalBinder extends Binder {

		public DatabaseHelper getDBHelpher() {
			return dbHelper;
		}
	}

	@Override
	public void onDestroy() {
		this.dbHelper = null;
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		dbHelper = new DatabaseHelper(this);
		return this.mBinder;
	}
	private static class DBServiceConnection implements ServiceConnection {

		private final DatabaseServiceConsumer consumer;

		public DBServiceConnection(DatabaseServiceConsumer consumer) {
			this.consumer = consumer;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			consumer.onDBServiceDisconnected();
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			consumer.onDBServiceConnected(((LocalBinder) binder).getDBHelpher());
		}
	};

	public static void bindService(Context context) {
		Intent intent = new Intent(context, DatabaseService.class);
		dbServiceCon = new DBServiceConnection((DatabaseServiceConsumer) context);
		context.bindService(intent, dbServiceCon, Context.BIND_AUTO_CREATE);
	}

	public static void unbindService(Context context) {
		if (dbServiceCon != null) context.unbindService(dbServiceCon);
	}
}
