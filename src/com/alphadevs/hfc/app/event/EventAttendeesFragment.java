package com.alphadevs.hfc.app.event;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.alphadevs.hfc.app.common.model.MVolunteer;
import com.alphadevs.hfc.app.services.Callback;
import com.alphadevs.hfc.app.volunteer.VolunteerProfileActivity;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;

public class EventAttendeesFragment extends Fragment implements EventProfileActivityEventsLinstner {

	ArrayList<MVolunteer> eventAttendeesList;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.following_volunteers, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		initEventGrid();
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		((EventPagerFragEventsLiserner) getActivity()).onPagerFragViewCreated(0);
	}
	private class AttendingEventsListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return eventAttendeesList == null ? 0 : eventAttendeesList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View eventAttendeesGridItem = layoutInflater.inflate(R.layout.volunteer_grid_item, parent, false);
			TextView eventAttendeesNameTV = (TextView) eventAttendeesGridItem.findViewById(R.id.volunteer_grid_volunteer_name);
			eventAttendeesNameTV.setText(eventAttendeesList.get(position).getName());
			TextView eventAttendeesLocTV = (TextView) eventAttendeesGridItem.findViewById(R.id.volunteer_grid_location_value);
			eventAttendeesLocTV.setText(eventAttendeesList.get(position).getLocation());
			return eventAttendeesGridItem;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(getActivity(), VolunteerProfileActivity.class);
			getActivity().startActivity(intent);
		}
	}

	@Override
	public void onEventHelperServiceConnected(EventProfileHelperService eventHelperService) {
		eventHelperService.getEventAttendingVolunteerList(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == EventProfileHelperService.FETCH_EVENT_ATTENDEES_SUCCESS) {
					String getEventAttendeesRespString = message.getData().getString("GET_EVENT_ATTENDEES_RESP_JSON");
					try {
						eventAttendeesList = (new ObjectMapper()).readValue(getEventAttendeesRespString, new TypeReference<ArrayList<MVolunteer>>() {});
						initEventGrid();
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void initEventGrid() {
		View eventAttendeesFragView = this.getView();
		if (eventAttendeesFragView == null || eventAttendeesList == null) { return; }
		GridView eventAttendeesGrid = (GridView) eventAttendeesFragView.findViewById(R.id.following_volunteers_grid);
		AttendingEventsListAdapter attendingEventsListAdapter = new AttendingEventsListAdapter();
		eventAttendeesGrid.setAdapter(attendingEventsListAdapter);
		eventAttendeesGrid.setOnItemClickListener(attendingEventsListAdapter);
	}
}
