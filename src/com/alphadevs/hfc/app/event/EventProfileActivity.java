package com.alphadevs.hfc.app.event;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.event.EventProfileHelperService.EventProfileHelperServiceBinder;
import com.hfc.app.R;

public class EventProfileActivity extends H4CFragmentActivity implements EventPagerFragEventsLiserner {

	ViewPager mViewPager;
	private EventProfileHelperService eventHelperService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_profile);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_event_profile, menu);
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();
		Intent intent = new Intent(this, EventProfileHelperService.class);
		bindService(intent, eventProfileHelperServiceCon, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(eventProfileHelperServiceCon);
		super.onStop();
	}

	private void initPagerFrags() {
		mViewPager = (ViewPager) findViewById(R.id.eventPager);
		mViewPager.setAdapter(new EventProfileAdapter(getSupportFragmentManager()));
		mViewPager.setCurrentItem(1);
	}
	public static class EventProfileAdapter extends FragmentPagerAdapter {

		private EventAttendeesFragment eventAttendeesFragment;
		private AboutFragment aboutEventFragment;
		private LocationFragment eventLocationFragment;

		public EventProfileAdapter(FragmentManager fm) {
			super(fm);
			eventAttendeesFragment = new EventAttendeesFragment();
			aboutEventFragment = new AboutFragment();
			eventLocationFragment = new LocationFragment();
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
					return eventAttendeesFragment;
				case 1:
					return aboutEventFragment;
				case 2:
					return eventLocationFragment;
				default:
					break;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "Attendees";
				case 1:
					return "About";
				case 2:
					return "Loaction";
				default:
					break;
			}
			return null;
		}

		public EventProfileActivityEventsLinstner getEventProfileActivityEventsLinstner(int position) {
			return (EventProfileActivityEventsLinstner) getItem(position);
		}
	}
	public static class AboutFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.ngo_about_ngo_fragment, container, false);
		}
	}
	public static class LocationFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.ngo_about_ngo_fragment, container, false);
		}
	}

	@Override
	public void onPagerFragViewCreated(int position) {
		EventProfileActivityEventsLinstner eventProfileActivityEventsLinstner = ((EventProfileAdapter) mViewPager.getAdapter()).getEventProfileActivityEventsLinstner(position);
		eventProfileActivityEventsLinstner.onEventHelperServiceConnected(eventHelperService);
	}
	public ServiceConnection eventProfileHelperServiceCon = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			eventHelperService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			eventHelperService = ((EventProfileHelperServiceBinder) service).getService();
			initPagerFrags();
		}
	};
}