package com.alphadevs.hfc.app.util;

import com.alphadevs.hfc.app.common.model.User;
import com.fasterxml.jackson.annotation.JsonProperty;

public class HFCAndroidSession {

	@JsonProperty("auth_token")
	private String authToken;
	@JsonProperty("user")
	private User user;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	/**
	 * @return the authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @param authToken
	 *            the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
