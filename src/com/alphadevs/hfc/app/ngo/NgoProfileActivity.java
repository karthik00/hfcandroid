package com.alphadevs.hfc.app.ngo;

import android.app.ActionBar;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.common.model.SearchResult;
import com.alphadevs.hfc.app.ngo.NgoHelperService.NgoHelperBinder;
import com.hfc.app.R;

public class NgoProfileActivity extends H4CFragmentActivity implements NgoPagerFragEventsLiserner {

	/**
	 * fragment used here should extend the NgoProfileActivityEventsLinstner
	 */
	private ViewPager mNgoProfileViewPager;
	private SearchResult ngo;
	private String ngoId;
	private NgoHelperService ngoHelperService;

	public NgoProfileActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		ngo = intent.getParcelableExtra("NGO");
		ngoId = intent.getStringExtra("NGO_ID");
		if (ngo != null) {
			setContentView(R.layout.ngo_profile);
			TextView ngoTitleView = (TextView) findViewById(R.id.ngo_profile_ngo_title_tv);
			ngoTitleView.setText(ngo.getName());
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Intent ngoHelperSerivceBindIntent = new Intent(this, NgoHelperService.class);
		bindService(ngoHelperSerivceBindIntent, ngoHelperServiceCon, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		unbindService(ngoHelperServiceCon);
		super.onStop();
	}
	private ServiceConnection ngoHelperServiceCon = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			ngoHelperService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			ngoHelperService = (NgoHelperService) ((NgoHelperBinder) service).getService();
			initFragmets();
		}
	};

	private void initFragmets() {
		mNgoProfileViewPager = (ViewPager) findViewById(R.id.ngoProfileHomeHSwipePager);
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
		getActionBar().setDisplayShowHomeEnabled(true);
		View homeIcon = findViewById(android.R.id.home);
		mNgoProfileViewPager.setAdapter(new NgoProfielSectionAdapter(getSupportFragmentManager()));
		NgoProfielSectionAdapter adapter = (NgoProfielSectionAdapter) mNgoProfileViewPager.getAdapter();
		mNgoProfileViewPager.setCurrentItem(1);
	}
	private class NgoProfielSectionAdapter extends FragmentPagerAdapter {

		private NgoProfileEventListFragment ngoProfileEventListFragment;
		private AboutNgoFragment aboutNgoFragment;
		private NgoProfileFollowersFragment ngoProfileFollowersFragment;

		public NgoProfielSectionAdapter(FragmentManager fm) {
			super(fm);
			ngoProfileFollowersFragment = new NgoProfileFollowersFragment();
			aboutNgoFragment = new AboutNgoFragment();
			ngoProfileEventListFragment = new NgoProfileEventListFragment();
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
				case 0:
					return ngoProfileFollowersFragment;
				case 1:
					return aboutNgoFragment;
				case 2:
					return ngoProfileEventListFragment;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "Followers";
				case 1:
					return "About";
				case 2:
					return "Events";
			}
			return null;
		}

		public NgoProfileActivityEventsLinstner getNgoProfileActivityEventsLinstner(int position) {
			return (NgoProfileActivityEventsLinstner) getItem(position);
		}
	}
	private class AboutNgoFragment extends Fragment implements NgoProfileActivityEventsLinstner {

		public AboutNgoFragment() {
			super();
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.ngo_about_ngo_fragment, null);
		}

		@Override
		public void onNgoHelperServiceConnected(NgoHelperService ngoHelperService) {}
	}

	@Override
	public void onPagerFragViewCreated(int position) {
		NgoProfileActivityEventsLinstner activityEventListenerFrag = ((NgoProfielSectionAdapter) mNgoProfileViewPager.getAdapter()).getNgoProfileActivityEventsLinstner(position);
		activityEventListenerFrag.onNgoHelperServiceConnected(ngoHelperService);
	}
}