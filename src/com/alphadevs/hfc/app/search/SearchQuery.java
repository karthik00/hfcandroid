package com.alphadevs.hfc.app.search;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SearchQuery {

	@JsonProperty
	private List<String> requirementsIds;
	@JsonProperty
	private List<String> fieldsOfActionIds;
	@JsonProperty
	private int pageNumber;
	@JsonProperty
	private int pageSize;

	public SearchQuery(String[] requirementsIds, String[] fieldsOfActionIds) {
		this.requirementsIds = Arrays.asList(requirementsIds);
		this.fieldsOfActionIds = Arrays.asList(fieldsOfActionIds);
		pageNumber = 1;
		pageSize = 10;
	}

	public String jsonAsString() {
		try {
			return (new ObjectMapper()).writeValueAsString(this);
		} catch (JsonProcessingException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<String> getRequirementsIds() {
		return requirementsIds;
	}

	public void setRequirementsIds(List<String> requirementsIds) {
		this.requirementsIds = requirementsIds;
	}

	public List<String> getFieldsOfActionIds() {
		return fieldsOfActionIds;
	}

	public void setFieldsOfActionIds(List<String> fieldsOfActionIds) {
		this.fieldsOfActionIds = fieldsOfActionIds;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
