package com.alphadevs.hfc.app.activity.main;

import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import com.alphadevs.hfc.app.util.HFCConstants;
import com.hfc.app.R;

public class FollowingNgosActivity extends H4CFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_following_ngos);
		((TextView) (findViewById(R.id.followingNgoTextView1))).setText(getIntent().getStringExtra(HFCConstants.FOLLOWING_NGO_KEY));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_following_ngos, menu);
		return true;
	}
}
