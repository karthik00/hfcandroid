package com.alphadevs.hfc.app.services;

import android.os.Message;

public interface Callback {

	void call(Message message);
}
