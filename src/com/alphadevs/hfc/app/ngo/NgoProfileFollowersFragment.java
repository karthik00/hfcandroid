package com.alphadevs.hfc.app.ngo;

import java.io.IOException;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.alphadevs.hfc.app.common.model.MVolunteer;
import com.alphadevs.hfc.app.services.Callback;
import com.alphadevs.hfc.app.volunteer.VolunteerProfileActivity;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;

public class NgoProfileFollowersFragment extends Fragment implements NgoProfileActivityEventsLinstner {

	private ArrayList<MVolunteer> ngoFollowers;
	private ProgressDialog mProgressBar;
	private GridView volunteerGrid;

	public NgoProfileFollowersFragment() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((NgoPagerFragEventsLiserner) getActivity()).onPagerFragViewCreated(0);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			ngoFollowers = savedInstanceState.getParcelableArrayList("NGO_FOLLOWERS_LIST");
		}
		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.ngo_followers_grid, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		setGridItems();
	}
	private class NgoFollowersListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return ngoFollowers == null ? 0 : ngoFollowers.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View volunteerGridItemView = layoutInflater.inflate(R.layout.volunteer_grid_item, parent, false);
			TextView volunteerGridVolunteerNameTV = (TextView) volunteerGridItemView.findViewById(R.id.volunteer_grid_volunteer_name);
			volunteerGridVolunteerNameTV.setText(((MVolunteer) ngoFollowers.get(position)).getName());
			TextView volunteerGridVolunteerLocTV = (TextView) volunteerGridItemView.findViewById(R.id.volunteer_grid_location_value);
			volunteerGridVolunteerLocTV.setText(((MVolunteer) ngoFollowers.get(position)).getLocation());
			return volunteerGridItemView;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(getActivity(), VolunteerProfileActivity.class);
			intent.putExtra("ID", ngoFollowers.get(position).getId());
			intent.putExtra("MVOLUNTEER", (MVolunteer) ngoFollowers.get(position));
			getActivity().startActivity(intent);
		}
	}

	@Override
	public void onNgoHelperServiceConnected(NgoHelperService ngoHelperService) {
		ngoHelperService.getNgoFollowers(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == NgoHelperService.FETCH_NGO_FOLLOWERS_SUCCESS) {
					String respJson = message.getData().getString("GET_NGO_FOLLOWERS_RESP_JSON");
					try {
						ngoFollowers = (new ObjectMapper()).readValue(respJson, new TypeReference<ArrayList<MVolunteer>>() {});
						setGridItems();
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void setGridItems() {
		View ngoFollowersFragView = NgoProfileFollowersFragment.this.getView();
		if (ngoFollowersFragView != null) {
			volunteerGrid = (GridView) ngoFollowersFragView.findViewById(R.id.ngo_followers_grid);
			NgoFollowersListAdapter ngoFollowesAdapter = new NgoFollowersListAdapter();
			volunteerGrid.setAdapter(ngoFollowesAdapter);
			volunteerGrid.setOnItemClickListener(ngoFollowesAdapter);
			ngoFollowesAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("NGO_FOLLOWERS_LIST", ngoFollowers);
	}
}
