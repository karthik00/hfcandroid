package com.alphadevs.hfc.app.common.model;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class MEvent implements Parcelable {

	// Please add the new fields to parcellable types
	@JsonProperty("id")
	@DatabaseField
	private String id;
	@JsonProperty("name")
	@DatabaseField
	private String name;
	@JsonProperty("objective")
	@DatabaseField
	private String objective;
	@JsonProperty("eventDate")
	@DatabaseField
	private Date eventDate;
	@JsonProperty("creationDate")
	@DatabaseField
	private Date creationDate;
	@JsonProperty("locations")
	@DatabaseField
	private String locations[];
	@JsonProperty("description")
	@DatabaseField
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String[] getLocations() {
		return locations;
	}

	public void setLocations(String[] locations) {
		this.locations = locations;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public MEvent() {
		// TODO Auto-generated constructor stub
	}

	private MEvent(Parcel in) {
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		// TODO Auto-generated method stub
		id = in.readString();
		name = in.readString();
		objective = in.readString();
		creationDate = (Date) in.readSerializable();
		eventDate = (Date) in.readSerializable();
		locations = in.createStringArray();
		description = in.readString();
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(objective);
		dest.writeSerializable(creationDate);
		dest.writeSerializable(eventDate);
		dest.writeStringArray(locations);
		dest.writeString(description);
	}
	public static final Parcelable.Creator<MEvent> CREATOR = new Creator<MEvent>() {

		@Override
		public MEvent[] newArray(int size) {
			// TODO Auto-generated method stub
			return new MEvent[size];
		}

		@Override
		public MEvent createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new MEvent(source);
		}
	};

	public CharSequence getLocationsAsString() {
		if (locations != null) {
			StringBuffer tempLoc = new StringBuffer("");
			for (String loc : locations) {
				tempLoc.append(loc + ",");
			}
		}
		return null;
	}
}
