package com.alphadevs.hfc.app.volunteer;


public interface VolunteerProfileActivityEventsListener {

	void onVolunteerProfileHelperServiceConnected(VolunteerProfileHelperService volunteerProfileHelperService);
}
