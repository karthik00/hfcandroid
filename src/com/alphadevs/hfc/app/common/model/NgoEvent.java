package com.alphadevs.hfc.app.common.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NgoEvent {

	@JsonProperty("id")
	private String id;
	@JsonProperty("objective")
	private String objective;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("locations")
	private String locations;
	@JsonProperty("eventDate")
	private Date eventDate;
	@JsonProperty("creationDate")
	private Date creationDate;

	public String getId() {
		return id;
	}

	public String getObjective() {
		return objective;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getLocations() {
		return locations;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}
}
