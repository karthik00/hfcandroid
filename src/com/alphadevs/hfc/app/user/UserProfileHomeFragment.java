package com.alphadevs.hfc.app.user;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hfc.app.R;

public class UserProfileHomeFragment extends Fragment {

	ViewPager mViewPager;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mViewPager = (ViewPager) getView().findViewById(R.id.userProfileHomeHSwipePager);
		mViewPager.setAdapter(new UserProfileFeedHomeHSwipeAdapter(this.getChildFragmentManager()));
		mViewPager.setCurrentItem(1);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.user_profile_home, null);
	}
	public static class UserProfileFeedHomeHSwipeAdapter extends FragmentPagerAdapter {

		public UserProfileFeedHomeHSwipeAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
			// TODO Auto-generated constructor stub
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			switch (position) {
				case 0:
					return new UserSuggestionsFeedFrgment();
				case 1:
					return new UserSuggestionsFeedFrgment();
				case 2:
					return new UserSuggestionsFeedFrgment();
				default:
					break;
			}
			return null;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
				case 0:
					return "Suggestions".toUpperCase();
				case 1:
					return "Near By".toUpperCase();
				case 2:
					return "Trending".toUpperCase();
				default:
					break;
			}
			return null;
		}
	}
	public static class UserSuggestionsFeedFrgment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.activity_volunteer_list, null);
		}
	}
	public static class UserNearbyFeedFrgment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.activity_events_list, null);
		}
	}
	public static class UserTrendingFeedFrgment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			return inflater.inflate(R.layout.activity_following_ngos, null);
		}
	}
}
