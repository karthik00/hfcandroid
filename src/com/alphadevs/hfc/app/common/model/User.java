package com.alphadevs.hfc.app.common.model;

import java.util.GregorianCalendar;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class User {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Users [ userId=" + userId + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", email=" + email + lastLoggedInDate + "]";
	}
	@JsonProperty("id")
	@DatabaseField
	private String userId;
	@JsonProperty("firstName")
	@DatabaseField
	private String firstName;
	@JsonProperty("middleName")
	@DatabaseField
	private String middleName;
	@JsonProperty("lastName")
	@DatabaseField
	private String lastName;
	@JsonProperty("email")
	@DatabaseField
	private String email;
	@JsonProperty("lastLoggedInDate")
	@DatabaseField
	private GregorianCalendar lastLoggedInDate;
	@JsonProperty("creationDate")
	@DatabaseField
	private GregorianCalendar creationDate;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the lastLoggedInDate
	 */
	public GregorianCalendar getLastLoggedInDate() {
		return lastLoggedInDate;
	}

	/**
	 * @return the creationDate
	 */
	public GregorianCalendar getCreationDate() {
		return creationDate;
	}
}
