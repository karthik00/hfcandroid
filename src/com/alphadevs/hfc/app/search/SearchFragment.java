package com.alphadevs.hfc.app.search;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.hfc.app.R;

public class SearchFragment extends Fragment implements OnClickListener {

	private static final int RESET_BUTTONS = -1;
	// views
	private MultiAutoCompleteTextView mMultiTextViewSearchReqs;
	private MultiAutoCompleteTextView mMultiTextViewFieldOfActions;
	private Spinner mSpinnerLocation;
	Button mButtonNgo, mButtonEvents;
	LinearLayout mLinearLayoutWorkingfor, mLinearLayoutLookingfor;
	RelativeLayout mRelativeLayoutLocations;
	private OnClickEventListener mOnClickEventListener;

	public SearchFragment() {
		// TODO Auto-generated constructor stub
		super();
	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mOnClickEventListener = (OnClickEventListener) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.searchlayout_ngo_events, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		initViews();
		((SearchFragEvetsListener) getActivity()).onSearchFragStarted();
	}

	private void initViews() {
		View view = getView();
		mMultiTextViewSearchReqs = (MultiAutoCompleteTextView) view.findViewById(R.id.search_requirements);
		mMultiTextViewFieldOfActions = (MultiAutoCompleteTextView) view.findViewById(R.id.search_field_of_actions);
		mSpinnerLocation = (Spinner) view.findViewById(R.id.search_locations_spinner);
		mButtonNgo = (Button) view.findViewById(R.id.search_button_searchNgo);
		mButtonEvents = (Button) view.findViewById(R.id.search_button_searchEvents);
		mLinearLayoutWorkingfor = (LinearLayout) view.findViewById(R.id.search_requirement_layout);
		mLinearLayoutLookingfor = (LinearLayout) view.findViewById(R.id.search_field_of_actions_layout);
		mRelativeLayoutLocations = (RelativeLayout) view.findViewById(R.id.location_layout);
		mMultiTextViewSearchReqs.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
		mMultiTextViewFieldOfActions.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
		mButtonNgo.setOnClickListener(this);
		mButtonEvents.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		toggleButtonStates(v.getId());
		showSearchOptions(false);
		switch (v.getId()) {
			case R.id.search_button_searchNgo:
				mOnClickEventListener.onButtonClick(SearchActivity.CLICKED_NGO);
				break;
			case R.id.search_button_searchEvents:
				mOnClickEventListener.onButtonClick(SearchActivity.CLICKED_EVENTS);
		}
		/*
		 * FragmentManager fragManager=
		 * getActivity().getSupportFragmentManager(); FragmentTransaction
		 * fragTrans = fragManager.beginTransaction();
		 * fragTrans.setCustomAnimations(R.animator.slide_in_left,
		 * R.animator.slide_in_right); SearchResultsFragment searchResultsFrag =
		 * new SearchResultsFragment();
		 * fragTrans.add(R.id.searchResultsFragment,
		 * searchResultsFrag,SearchActivity.SEARCH_RESULTS_TAG);
		 * fragTrans.commit();
		 */
	}

	private void toggleButtonStates(int id) {
		switch (id) {
			case R.id.search_button_searchNgo:
				mButtonNgo.setEnabled(false);
				mButtonEvents.setEnabled(true);
				break;
			case R.id.search_button_searchEvents:
				mButtonNgo.setEnabled(true);
				mButtonEvents.setEnabled(false);
				break;
			case RESET_BUTTONS:
				mButtonNgo.setEnabled(true);
				mButtonEvents.setEnabled(true);
		}
	}

	private void showSearchOptions(boolean isShow) {
		if (isShow) {
			mLinearLayoutLookingfor.setVisibility(View.VISIBLE);
			mLinearLayoutWorkingfor.setVisibility(View.VISIBLE);
			mRelativeLayoutLocations.setVisibility(View.VISIBLE);
			FragmentManager fragManager = getActivity().getSupportFragmentManager();
			FragmentTransaction fragTrans = fragManager.beginTransaction();
			/*
			 * //remove results fragment
			 * if(fragManager.findFragmentByTag(SearchActivity
			 * .SEARCH_RESULTS_TAG) != null)
			 * fragTrans.remove(fragManager.findFragmentByTag
			 * (SearchActivity.SEARCH_RESULTS_TAG));
			 */
			return;
		}
		mLinearLayoutLookingfor.setVisibility(View.GONE);
		mLinearLayoutWorkingfor.setVisibility(View.GONE);
		mRelativeLayoutLocations.setVisibility(View.GONE);
	}

	public boolean onBackPressed() {
		if (mLinearLayoutLookingfor != null && mLinearLayoutLookingfor.getVisibility() == View.GONE) {
			showSearchOptions(true);
			toggleButtonStates(RESET_BUTTONS);
			return true;
		}
		return false;
	}
}
