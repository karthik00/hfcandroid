package com.alphadevs.hfc.app.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.alphadevs.hfc.app.event.EventProfileActivity;
import com.alphadevs.hfc.app.util.HFCConstants;
import com.hfc.app.R;


public class FollowingEventsActivity extends H4CFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_following_events);
		((TextView) (findViewById(R.id.followingEventsTextView1))).setText(getIntent().getStringExtra(HFCConstants.FollwinEventsKey));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_following_events, menu);
		return true;
	}

	public void onEventDetailsClickListener(View view) {
		Intent eventDetailsIntent = new Intent(this, EventProfileActivity.class);
		eventDetailsIntent.putExtra(HFCConstants.EVENT_KEY, "eventSampleKey");
		startActivity(eventDetailsIntent);
		;
	}
}
