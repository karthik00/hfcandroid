package com.alphadevs.hfc.app.user;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.alphadevs.hfc.app.util.HFCConstants;

public class UserConnectionsPagerAdapter extends FragmentPagerAdapter {

	public UserConnectionsPagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {
			case HFCConstants.USER_FOLLOWING_VOLUNTEERS_TAB_INDEX:
				return new UserFollowingVolunteers();
			case HFCConstants.USER_ATTENDING_EVENTS_TAB_INDEX:
				return new UserAttendingEvents();
			case HFCConstants.USER_FOLLOWING_NGOS_TAB_INDEX:
				return new UserFollowingNgos();
			default:
				return null;
		}
	}

	@Override
	public int getCount() {
		return HFCConstants.USER_CONNECTION_TAB_TITLE_COUNT;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
			case HFCConstants.USER_FOLLOWING_VOLUNTEERS_TAB_INDEX:
				return HFCConstants.USER_FOLLOWING_VOLUNTEERS_TAB_TITLE;
			case HFCConstants.USER_FOLLOWING_NGOS_TAB_INDEX:
				return HFCConstants.USER_FOLLOWING_NGOS_TAB_TITLE;
			case HFCConstants.USER_ATTENDING_EVENTS_TAB_INDEX:
				return HFCConstants.USER_ATTENDING_EVENTS_TAB_TITLE;
			default:
				return null;
		}
	}
}
