package com.alphadevs.hfc.app.common.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Ngo {

	@DatabaseField(id = true)
	int id;

	@Override
	public String toString() {
		return "Ngo [id=" + id + ", name=" + name + ", mission=" + mission + ", vission=" + vission + "]";
	}
	@DatabaseField
	String name;
	@DatabaseField
	String mission;
	@DatabaseField
	String vission;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMission() {
		return mission;
	}

	public void setMission(String mission) {
		this.mission = mission;
	}

	public String getVission() {
		return vission;
	}

	public void setVission(String vission) {
		this.vission = vission;
	}

	public int getId() {
		return id;
	}
}
