package com.alphadevs.hfc.app.event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.services.Callback;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class EventProfileHelperService extends Service {

	protected static final int FETCH_EVENT_ATTENDEES_SUCCESS = 0;
	AsyncHttpClient wsClient = new AsyncHttpClient();
	@Override
	public IBinder onBind(Intent intent) {
		return new EventProfileHelperServiceBinder();
	}
	public class EventProfileHelperServiceBinder extends Binder {

		public EventProfileHelperService getService() {
			return EventProfileHelperService.this;
		}
	}

	public void getEventAttendingVolunteerList(final Callback callback) {
		wsClient.get("http://127.0.0.1/xx", new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				StringBuffer jsonString = null;
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("ngoFollowers.json")));
					jsonString = new StringBuffer();
					String temp = br.readLine();
					while (temp != null) {
						jsonString.append(temp + "\n");
						temp = br.readLine();
					}
					jsonString.append(temp + "\n");
					onSuccess(jsonString.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(String responseJson) {
				super.onSuccess(responseJson);
				Message message = new Message();
				message.what = EventProfileHelperService.FETCH_EVENT_ATTENDEES_SUCCESS;
				Bundle resultBundle = new Bundle();
				resultBundle.putString("GET_EVENT_ATTENDEES_RESP_JSON", responseJson);
				message.setData(resultBundle);
				callback.call(message);
			}
		});
	}
}
