package com.alphadevs.hfc.app.search;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.hfc.app.R;

public class SearchResultsFragment extends Fragment {

	private ListView mListview;
	private ProgressBar mProgressLoadingResults;

	public SearchResultsFragment() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.search_results, container, false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initViews();
		mProgressLoadingResults.setVisibility(View.VISIBLE);
		mProgressLoadingResults.setVisibility(View.GONE);
		Message message = new Message();
		((SearchResultFragEventListener) getActivity()).onSearchResultFragAttched();
	}

	private void initViews() {
		View view = getView();
		mListview = (ListView) view.findViewById(R.id.search_results_list);
		mProgressLoadingResults = (ProgressBar) view.findViewById(R.id.search_loading_results);
	}
}
