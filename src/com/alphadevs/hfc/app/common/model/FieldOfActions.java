package com.alphadevs.hfc.app.common.model;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class FieldOfActions implements Parcelable {

	@JsonProperty("id")
	@DatabaseField(id = true)
	String id;
	@JsonProperty("name")
	@DatabaseField
	String name;
	@JsonProperty("description")
	@DatabaseField(canBeNull = true)
	String description;
	@JsonProperty("creationDate")
	@DatabaseField(canBeNull = true)
	Date creationDate;
	@JsonProperty("lastUpdated")
	@DatabaseField(canBeNull = true)
	Date lastUpdated;

	public FieldOfActions() {
		// Default constructor json
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public String toString() {
		return "FieldOfActions [id=" + id + ", name=" + name + ", description=" + description + ", creationDate=" + creationDate + ", lastUpdated=" + lastUpdated + "]";
	}

	@Override
	public int describeContents() {
		return 0;
	}
	public static final Parcelable.Creator<FieldOfActions> CREATOR = new Creator<FieldOfActions>() {

		@Override
		public FieldOfActions[] newArray(int size) {
			return new FieldOfActions[size];
		}

		@Override
		public FieldOfActions createFromParcel(Parcel in) {
			return new FieldOfActions(in);
		}
	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeSerializable(creationDate);
		dest.writeSerializable(lastUpdated);
	}

	public FieldOfActions(Parcel in) {
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		id = in.readString();
		name = in.readString();
		description = in.readString();
		creationDate = (Date) in.readSerializable();
		lastUpdated = (Date) in.readSerializable();
	}
}
