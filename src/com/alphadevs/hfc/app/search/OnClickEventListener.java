package com.alphadevs.hfc.app.search;

public interface OnClickEventListener {

	public void onButtonClick(int clickedWhat);
}