package com.alphadevs.hfc.app.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.alphadevs.hfc.app.util.HFCConstants;
import com.alphadevs.hfc.app.volunteer.VolunteerProfileActivity;
import com.hfc.app.R;

public class FollowingVolunteesActivity extends H4CFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_following_voluntees);
		((TextView) (findViewById(R.id.followingVolunteersTextView1))).setText(getIntent().getStringExtra(HFCConstants.FOLLOWING_VOLUNTEERS_KEY));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_following_voluntees, menu);
		return true;
	}

	public void onGetVolunteerProfileBtnClick(View view) {
		Intent intent = new Intent(this, VolunteerProfileActivity.class);
		intent.putExtra(HFCConstants.VOLUNTEER_KEY, "Volunteer key");
		startActivity(intent);
		;
	}
}
