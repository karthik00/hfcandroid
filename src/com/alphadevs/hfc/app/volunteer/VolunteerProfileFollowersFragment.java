package com.alphadevs.hfc.app.volunteer;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.alphadevs.hfc.app.common.model.MVolunteer;
import com.alphadevs.hfc.app.ngo.NgoPagerFragEventsLiserner;
import com.hfc.app.R;

public class VolunteerProfileFollowersFragment extends Fragment {

	private ArrayList<MVolunteer> volunteerFollowers;
	private ProgressDialog mProgressBar;
	private GridView volunteerGrid;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((NgoPagerFragEventsLiserner) getActivity()).onPagerFragViewCreated(0);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			volunteerFollowers = savedInstanceState.getParcelableArrayList("NGO_FOLLOWERS_LIST");

		}
		super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate(R.layout.ngo_followers_grid, container, false);
	}

	@Override
	public void onStart() {
		super.onStart();
		setGridItems();
	}
	private class VolunteerFollowersListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return volunteerFollowers == null ? 0 : volunteerFollowers.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View volunteerGridItemView = layoutInflater.inflate(R.layout.volunteer_grid_item, parent, false);
			return volunteerGridItemView;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(getActivity(), VolunteerProfileActivity.class);
			intent.putExtra("MVOLUNTEER", volunteerFollowers.get(position));
			getActivity().startActivity(intent);
		}
	}


	/**
	public void onNgoHelperServiceConnected(NgoHelperService ngoHelperService) {
		ngoHelperService.getNgoFollowers(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == NgoHelperService.FETCH_NGO_FOLLOWERS_SUCCESS) {
					String respJson = message.getData().getString("GET_NGO_FOLLOWERS_RESP_JSON");
					try {
						ngoFollowers = (new ObjectMapper()).readValue(respJson, new TypeReference<ArrayList<MVolunteer>>() {});
						setGridItems();
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}*/

	private void setGridItems() {
		View volunteerFollowersFragView = this.getView();
		if (volunteerFollowersFragView != null) {
			volunteerGrid = (GridView) volunteerFollowersFragView.findViewById(R.id.ngo_followers_grid);
			VolunteerFollowersListAdapter ngoFollowesAdapter = new VolunteerFollowersListAdapter();
			volunteerGrid.setAdapter(ngoFollowesAdapter);
			volunteerGrid.setOnItemClickListener(ngoFollowesAdapter);
			ngoFollowesAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("NGO_FOLLOWERS_LIST", volunteerFollowers);
	}
}
