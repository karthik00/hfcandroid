package com.alphadevs.hfc.app.common.model;

import java.net.URL;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Contact {

	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	List<String> perName;
	@DatabaseField
	List<String> email;
	@DatabaseField
	List<URL> url;
	@DatabaseField
	String address1;
	@DatabaseField
	String address2;
	@DatabaseField
	String address3;
	@DatabaseField
	String landMark;
	@DatabaseField
	String street;
	@DatabaseField
	String area;
	@DatabaseField
	String city;
	@DatabaseField
	int pincode;
}
