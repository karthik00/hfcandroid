package com.alphadevs.hfc.app.volunteer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.alphadevs.hfc.app.util.HFCConstants;
import com.hfc.app.R;

public class VolunteerListActivity extends H4CFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_volunteer_list);
		((TextView) (findViewById(R.id.volunteerlistTextView1))).setText(getIntent().getStringExtra(HFCConstants.VOLUNTEER_LIST_KEY));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_volunteer_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// This ID represents the Home or Up button. In the case of this
				// activity, the Up button is shown. Use NavUtils to allow users
				// to navigate up one level in the application structure. For
				// more details, see the Navigation pattern on Android Design:
				//
				// http://developer.android.com/design/patterns/navigation.html#up-vs-back
				//
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onViewVolunteerProfileListener(View view) {
		Intent volunteerDetailsIntent = new Intent(this, VolunteerProfileActivity.class);
		volunteerDetailsIntent.putExtra(HFCConstants.VOLUNTEER_KEY, "sample_vol_key");
		startActivity(volunteerDetailsIntent);
	}
}
