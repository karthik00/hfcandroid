package com.alphadevs.hfc.app.core.db;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.alphadevs.hfc.app.common.model.FieldOfActions;
import com.alphadevs.hfc.app.common.model.SearchRequirements;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DB_NAME = "H4CLiteDB.sqlite";
	private static final int DB_VERSION = 1;

	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource conSource) {
		try {
			TableUtils.createTableIfNotExists(conSource, SearchRequirements.class);
			TableUtils.createTableIfNotExists(conSource, FieldOfActions.class);
		} catch (SQLException sqlEx) {
			Log.e(DatabaseHelper.class.getName(), "Cann't create DB", sqlEx);
		}
	}

	@Override
	public <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) {
		try {
			return super.getDao(clazz);
		} catch (SQLException e) {
			// HANDLE EXCEPTION
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2, int arg3) {}
}