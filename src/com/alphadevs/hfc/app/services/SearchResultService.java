package com.alphadevs.hfc.app.services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.search.SearchQuery;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SearchResultService extends Service {

	private AsyncHttpClient webClient = new AsyncHttpClient();
	public class SearchRestultBinder extends Binder {

		public SearchResultService getService() {
			return SearchResultService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return new SearchRestultBinder();
	}
	public interface SearchResponse {

		int SUCCESS = 1;
		int FAILED = 0;
	}
	StringBuilder searchResult;

	public void getNgoSearchResult(final Callback callback, SearchQuery searchQuery) {
		webClient.post("http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login", null, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				try {
					InputStream inputStream = getAssets().open("searchResult.json");
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
					searchResult = new StringBuilder("");
					String temp = bufferedReader.readLine();
					while (temp != null) {
						searchResult.append(temp + "\n");
						temp = bufferedReader.readLine();
					}
					System.out.println("########" + searchResult + "########");
					onSuccess(searchResult.toString());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				super.onFailure(arg0, arg1);
			}

			@Override
			public void onSuccess(String searchResultString) {
				Message message = new Message();
				super.onSuccess(searchResultString);
				message.what = SearchResponse.SUCCESS;
				Bundle resultBundle = new Bundle();
				resultBundle.putString("SearchResult", searchResultString);
				message.setData(resultBundle);
				callback.call(message);
			}
		});
	}
}
