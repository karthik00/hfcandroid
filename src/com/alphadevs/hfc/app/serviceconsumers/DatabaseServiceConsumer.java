package com.alphadevs.hfc.app.serviceconsumers;

import com.alphadevs.hfc.app.core.db.DatabaseHelper;

public interface DatabaseServiceConsumer {

	void onDBServiceDisconnected();

	void onDBServiceConnected(DatabaseHelper dbHelpher);
}
