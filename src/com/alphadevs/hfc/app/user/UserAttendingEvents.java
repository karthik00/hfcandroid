package com.alphadevs.hfc.app.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.alphadevs.hfc.app.event.EventProfileActivity;
import com.hfc.app.R;

public class UserAttendingEvents extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.attending_events, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		GridView eventsGrid = (GridView) this.getView().findViewById(R.id.following_events_grid);
		AttendingEventsListAdapter attendingEventsListAdapter = new AttendingEventsListAdapter();
		eventsGrid.setAdapter(attendingEventsListAdapter);
		eventsGrid.setOnItemClickListener(attendingEventsListAdapter);
		super.onActivityCreated(savedInstanceState);
	}
	private class AttendingEventsListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return 20;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = layoutInflater.inflate(R.layout.event_grid_item, parent, false);
			return view;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			Intent intent = new Intent(getActivity(), EventProfileActivity.class);
			getActivity().startActivity(intent);
		}
	}
}
