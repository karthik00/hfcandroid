package com.alphadevs.hfc.app.serviceconsumers;

import com.alphadevs.hfc.app.services.HFCServerCommunicationServices;

public interface HfcWSClientConsumer {

	void onServiceConnected(HFCServerCommunicationServices hfcServerCommunicationServices);

	void onServiceDisconnected();
}
