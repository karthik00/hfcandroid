package com.alphadevs.hfc.app.activity.main;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.alphadevs.hfc.app.search.SearchActivity;
import com.alphadevs.hfc.app.util.HFCAndroidSessionManager;
import com.hfc.app.R;

public class H4CFragmentActivity extends FragmentActivity {

	public static final int LOG_IN = Menu.FIRST + 1;
	public static final int LOG_OUT = Menu.FIRST + 2;

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actionbar_menu_item, menu);
		if (HFCAndroidSessionManager.isSessionActive() && menu.findItem(LOG_OUT) == null) menu.add(Menu.NONE, LOG_OUT, Menu.NONE, R.string.menu_logout);
		else if (!HFCAndroidSessionManager.isSessionActive() && menu.findItem(LOG_IN) == null) menu.add(Menu.NONE, LOG_IN, Menu.NONE, R.string.menu_logout);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_search:
				startActivity(new Intent(this, SearchActivity.class));
				break;
			case LOG_IN:
				startActivity(new Intent(this, H4CMainActivity.class));
				break;
			case LOG_OUT:
				HFCAndroidSessionManager.endSession();
				Intent i = new Intent(this, H4CMainActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(i);
				break;
		}
		return super.onOptionsItemSelected(item);
	}
}
