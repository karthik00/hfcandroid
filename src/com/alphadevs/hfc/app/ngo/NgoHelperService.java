package com.alphadevs.hfc.app.ngo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.services.Callback;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class NgoHelperService extends Service {

	protected static final int FETCH_NGO_EVETS_SUCCESS = 0;
	protected static final int FETCH_NGO_FOLLOWERS_SUCCESS = 0;

	@Override
	public IBinder onBind(Intent arg0) {
		return new NgoHelperBinder();
	}
	public class NgoHelperBinder extends Binder {
		public NgoHelperService getService(){
			return NgoHelperService.this;
		}
	}
	private AsyncHttpClient wsClient = new AsyncHttpClient();

	public void getNgoFollowers(final Callback callback) {
		wsClient.get("http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login?", new AsyncHttpResponseHandler() {
			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				StringBuffer jsonString = null;
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("ngoFollowers.json")));
					jsonString = new StringBuffer();
					String temp = br.readLine();
					while (temp != null) {
						jsonString.append(temp + "\n");
						temp = br.readLine();
					}
					jsonString.append(temp + "\n");
					onSuccess(jsonString.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			@Override
			public void onSuccess(String responseJson) {
				super.onSuccess(responseJson);
				Message message = new Message();
				message.what = NgoHelperService.FETCH_NGO_FOLLOWERS_SUCCESS;
				Bundle resultBundle = new Bundle();
				resultBundle.putString("GET_NGO_FOLLOWERS_RESP_JSON", responseJson);
				message.setData(resultBundle);
				callback.call(message);
			}
		});
	}

	public void getNgoEvents(final Callback callback) {
		wsClient.get("http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login?", new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				StringBuffer jsonString = null;
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("ngoEvents.json")));
					jsonString = new StringBuffer();
					String temp = br.readLine();
					while (temp != null) {
						jsonString.append(temp + "\n");
						temp = br.readLine();
					}
					jsonString.append(temp + "\n");
					onSuccess(jsonString.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(String responseJson) {
				super.onSuccess(responseJson);
				Message message = new Message();
				message.what = NgoHelperService.FETCH_NGO_EVETS_SUCCESS;
				Bundle resultBundle = new Bundle();
				resultBundle.putString("GET_NGO_EVENTS_RESPONSE_JSON", responseJson);
				message.setData(resultBundle);
				callback.call(message);
			}
		});
	}
}
