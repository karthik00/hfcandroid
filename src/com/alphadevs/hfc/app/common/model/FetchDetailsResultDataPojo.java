package com.alphadevs.hfc.app.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FetchDetailsResultDataPojo {

	@JsonProperty("requirements")
	private SearchRequirements[] searchRequirements;
	@JsonProperty("fields_of_action")
	private FieldOfActions[] fieldOfActions;

	@Override
	public String toString() {
		return "FetchDetailsResultDataPojo [requirements=" + searchRequirements.length + ", fieldOfActions=" + fieldOfActions[0].toString() + "]";
	}

	public SearchRequirements[] getSearchRequirements() {
		return searchRequirements;
	}

	public FieldOfActions[] getFieldOfActions() {
		return fieldOfActions;
	}
}
