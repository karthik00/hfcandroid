package com.alphadevs.hfc.app.volunteer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.services.Callback;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;


public class VolunteerProfileHelperService extends Service {

	protected static final int FETCH_VOLUNTEER_ATTENDING_EVENTS_SUCCESS = 0;
	protected static final int FETCH_VOLUNTEER_FOLLOWING_NGOS_SUCCESS = 0;

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return new VolunteerProfileServiceBinder();
	}
	public class VolunteerProfileServiceBinder extends Binder {

		public VolunteerProfileHelperService getService() {
			return VolunteerProfileHelperService.this;
		}
	}
	AsyncHttpClient wsClient = new AsyncHttpClient();

	public void getVolunteerFollowingNgos(final Callback callback) {
		wsClient.get("http://127.0.0.1:8080/rest-api-1.0/login?", new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				StringBuffer jsonString = null;
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("followingNgos.json")));
					jsonString = new StringBuffer();
					String temp = br.readLine();
					while (temp != null) {
						jsonString.append(temp + "\n");
						temp = br.readLine();
					}
					jsonString.append(temp + "\n");
					onSuccess(jsonString.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(String respJsonString) {
				super.onSuccess(respJsonString);
				Message message = new Message();
				message.what = VolunteerProfileHelperService.FETCH_VOLUNTEER_FOLLOWING_NGOS_SUCCESS;
				Bundle data = new Bundle();
				data.putString("VOLUNTEER_FOLLOWING_NGOS_JSON", respJsonString);
				message.setData(data);
				callback.call(message);
			}
		});
	}

	public void getVolunteerAttendingEvents(final Callback callback) {
		wsClient.get("http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login?", new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				StringBuffer jsonString = null;
				try {
					BufferedReader br = new BufferedReader(new InputStreamReader(getAssets().open("ngoEvents.json")));
					jsonString = new StringBuffer();
					String temp = br.readLine();
					while (temp != null) {
						jsonString.append(temp + "\n");
						System.out.println(temp);
						temp = br.readLine();
					}
					jsonString.append(temp + "\n");
					onSuccess(jsonString.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onSuccess(String respJsonString) {
				Message message = new Message();
				message.what = VolunteerProfileHelperService.FETCH_VOLUNTEER_ATTENDING_EVENTS_SUCCESS;
				Bundle data = new Bundle();
				data.putString("VOLUNTEER_ATTENDING_EVENTS_JSON", respJsonString);
				message.setData(data);
				callback.call(message);
			}
		});
	}
}
