package com.alphadevs.hfc.app.services;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.util.HFCAndroidSessionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AuthenticationService extends Service {

	private AsyncHttpClient web_client = new AsyncHttpClient();
	public class AuthenticationBinder extends Binder {

		public AuthenticationService getService() {
			return AuthenticationService.this;
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return new AuthenticationBinder();
	}
	public class AuthenticationResponse {

		public static final int SUCCESS = 1;
		public static final int FAILED = 0;
	}
	private String loginJsonRes =
			"{  \"status\" : \"SUCCESS\",   \"data\" : {    \"auth_token\" : \"50e7bc2de4b078153882eecd\",    \"user\" : {      \"firstName\" : null,      \"id\" : \"50e7ba3be4b0c259dd99379c\",      \"creationDate\" : 1357363771565,      \"middleName\" : null,      \"email\" : \"karthik001@me.com\",      \"lastLoggedInDate\" : 1357364269259,      \"lastName\" : null    }}}";
	private static final String LOGIN_URL = "http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login";

	public void login(final Callback callback, RequestParams params) {
		web_client.post(LOGIN_URL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				// TODO Auto-generated method stub
				super.onFailure(arg0, arg1);
				onSuccess(loginJsonRes);
			}

			@Override
			public void onSuccess(String response) {
				Message message = new Message();
				JSONObject loginResult = null;
				try {
					loginResult = new JSONObject(response);
					if (loginResult.getString("status").equalsIgnoreCase("SUCCESS")) {
						message.what = AuthenticationResponse.SUCCESS;
						HFCAndroidSessionManager.startSession(getApplicationContext(), loginResult);
					} else {
						message.what = AuthenticationResponse.FAILED;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				callback.call(message);
			}
		});
	}

	public void loginWithFB(Callback callback, RequestParams params) {}
}
