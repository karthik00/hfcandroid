package com.alphadevs.hfc.app.volunteer;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.alphadevs.hfc.app.common.model.MEvent;
import com.alphadevs.hfc.app.event.EventProfileActivity;
import com.alphadevs.hfc.app.services.Callback;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hfc.app.R;

public class VolunteerAttendingEvents extends Fragment implements VolunteerProfileActivityEventsListener {

	ArrayList<MEvent> volunteerAttendingEventsList;

	public VolunteerAttendingEvents() {
		super();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((VolunteerProfilePagerFragEventsLiserner) getActivity()).onFragCreated(2);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			volunteerAttendingEventsList = savedInstanceState.getParcelableArrayList("VOLUNTEER_ATTENDING_EVENTS_LIST");
		}
		return inflater.inflate(R.layout.attending_events, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
		initEventGridItems();
	}
	private class AttendingEventsListAdapter extends BaseAdapter implements OnItemClickListener {

		@Override
		public int getCount() {
			return volunteerAttendingEventsList == null ? 0 : volunteerAttendingEventsList.size();
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View eventGridItemsView = layoutInflater.inflate(R.layout.event_grid_item, parent, false);
			TextView eventGridEventNameTV = (TextView) eventGridItemsView.findViewById(R.id.event_grid_event_name_tv);
			eventGridEventNameTV.setText(volunteerAttendingEventsList.get(position).getName());
			TextView eventGridEventLocTV = (TextView) eventGridItemsView.findViewById(R.id.event_grid_event_location_tv);
			eventGridEventLocTV.setText(volunteerAttendingEventsList.get(position).getLocationsAsString());
			TextView eventGridEventDateTV = (TextView) eventGridItemsView.findViewById(R.id.event_grid_event_date_tv);
			eventGridEventLocTV.setText(DateFormat.format("MMM dd, yyyy h:mmaa", volunteerAttendingEventsList.get(position).getEventDate()));
			return eventGridItemsView;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent eventProfileIntent = new Intent(getActivity(), EventProfileActivity.class);
			eventProfileIntent.putExtra("EVENT_ID", volunteerAttendingEventsList.get(position).getId());
			eventProfileIntent.putExtra("M_EVENT", volunteerAttendingEventsList.get(position));
			getActivity().startActivity(eventProfileIntent);
		}
	}

	@Override
	public void onVolunteerProfileHelperServiceConnected(VolunteerProfileHelperService volunteerProfileHelperService) {
		volunteerProfileHelperService.getVolunteerAttendingEvents(new Callback() {

			@Override
			public void call(Message message) {
				if (message.what == VolunteerProfileHelperService.FETCH_VOLUNTEER_ATTENDING_EVENTS_SUCCESS) {
					String volunteerAttendingEventsJsonString = message.getData().getString("VOLUNTEER_ATTENDING_EVENTS_JSON");
					try {
						volunteerAttendingEventsList = (new ObjectMapper()).readValue(volunteerAttendingEventsJsonString, new TypeReference<ArrayList<MEvent>>() {});
						initEventGridItems();
					} catch (JsonParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
	}

	private void initEventGridItems() {
		View volunteerAttendingEventFragView = this.getView();
		if (volunteerAttendingEventFragView != null) {
			GridView volunteerAttendingEventsGrid = (GridView) volunteerAttendingEventFragView.findViewById(R.id.volunteer_attending_events_grid);
			volunteerAttendingEventsGrid.setAdapter(new AttendingEventsListAdapter());
			AttendingEventsListAdapter attendingEventsListAdapter = (AttendingEventsListAdapter) volunteerAttendingEventsGrid.getAdapter();
			volunteerAttendingEventsGrid.setOnItemClickListener(attendingEventsListAdapter);
			attendingEventsListAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList("VOLUNTEER_ATTENDING_EVENTS_LIST", volunteerAttendingEventsList);
	}
}