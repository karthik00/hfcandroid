package com.alphadevs.hfc.app.user;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.alphadevs.hfc.app.activity.main.H4CFragmentActivity;
import com.hfc.app.R;

public class UserConnections extends H4CFragmentActivity {

	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_connections_activity);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mViewPager = (ViewPager) findViewById(R.id.userConnectonPager);
		mViewPager.setAdapter(new UserConnectionsPagerAdapter(getSupportFragmentManager()));
		mViewPager.setCurrentItem(1);
	}
}
