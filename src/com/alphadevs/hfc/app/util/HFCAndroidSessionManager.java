package com.alphadevs.hfc.app.util;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.alphadevs.hfc.app.common.model.User;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HFCAndroidSessionManager {

	private static final String sessionSharedPrefName = "sessionSharedPref";
	private SharedPreferences sessionSharedPref;
	private Editor sessionEditor;
	private static HFCAndroidSessionManager sessionManager;

	private HFCAndroidSessionManager(Context applicationContext) {
		sessionSharedPref = applicationContext.getSharedPreferences(sessionSharedPrefName, Context.MODE_PRIVATE);
		sessionEditor = sessionSharedPref.edit();
	}

	public static Boolean startSession(Context applicationContext, JSONObject loginResult) throws JSONException {
		if (sessionManager == null) sessionManager = new HFCAndroidSessionManager(applicationContext);
		if (!HFCConstants.LOGIN_SUCCESS_STATUS.equalsIgnoreCase(loginResult.getString("status"))) return false;
		try {
			(new ObjectMapper()).readValue(loginResult.getString("data"), HFCAndroidSession.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		sessionManager.sessionEditor.putString("SessionData", loginResult.getString("data"));
		return sessionManager.sessionEditor.commit();
	}

	public static void endSession() {
		if (sessionManager != null) {
			sessionManager.sessionEditor.clear();
			sessionManager.sessionEditor.commit();
		}
	}

	public static boolean isSessionActive() {
		if (sessionManager == null || sessionManager.sessionSharedPref == null || sessionManager.sessionSharedPref.getString("SessionData", null) == null) return false;
		else return true;
	}

	public static User getSessionUser() {
		if (HFCAndroidSessionManager.isSessionActive()) return null;
		else {
			HFCAndroidSession session;
			try {
				session = (new ObjectMapper()).readValue(sessionManager.sessionSharedPref.getString("SessionData", null), HFCAndroidSession.class);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
			return session.getUser();
		}
	}
}
