package com.alphadevs.hfc.app.common.model;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchResult implements Parcelable {

	@JsonProperty("id")
	String id;
	@JsonProperty("vision")
	String vision;
	@JsonProperty("requirements")
	SearchRequirements requirements[];
	@JsonProperty("locations")
	String locations[];
	@JsonProperty("description")
	String description;
	@JsonProperty("fieldsOfAction")
	FieldOfActions fieldOfActions[];
	@JsonProperty("name")
	String name;
	@JsonProperty("mission")
	String mission;

	public SearchResult() {
		// deafult constructor for json handling
	}

	@Override
	public String toString() {
		return "SearchResult [id=" + id + ", vision=" + vision + ", requirements=" + Arrays.toString(requirements) + ", locations=" + Arrays.toString(locations) + ", description=" + description
				+ ", fieldOfActions=" + Arrays.toString(fieldOfActions) + ", name=" + name + ", mission=" + mission + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVision() {
		return vision;
	}

	public void setVision(String vision) {
		this.vision = vision;
	}

	public SearchRequirements[] getRequirements() {
		return requirements;
	}

	public void setRequirements(SearchRequirements[] requirements) {
		this.requirements = requirements;
	}

	public String[] getLocations() {
		return locations;
	}

	public void setLocations(String[] locations) {
		this.locations = locations;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FieldOfActions[] getFieldOfActions() {
		return fieldOfActions;
	}

	public void setFieldOfActions(FieldOfActions[] fieldOfActions) {
		this.fieldOfActions = fieldOfActions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMission() {
		return mission;
	}

	public void setMission(String mission) {
		this.mission = mission;
	}

	public SearchResult(Parcel in) {
		// constructor for parcellable
		readFromParcel(in);
	}

	private void readFromParcel(Parcel in) {
		id = in.readString();
		vision = in.readString();
		requirements = in.createTypedArray(SearchRequirements.CREATOR);
		locations = in.createStringArray();
		description = in.readString();
		fieldOfActions = in.createTypedArray(FieldOfActions.CREATOR);
		name = in.readString();
		mission = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(vision);
		dest.writeTypedArray(requirements, flags);
		dest.writeStringArray(locations);
		dest.writeString(description);
		dest.writeTypedArray(fieldOfActions, flags);
		dest.writeString(name);
		dest.writeString(mission);
	}
	public static final Parcelable.Creator<SearchResult> CREATOR = new Creator<SearchResult>() {

		@Override
		public SearchResult[] newArray(int size) {
			return new SearchResult[size];
		}

		@Override
		public SearchResult createFromParcel(Parcel in) {
			return new SearchResult(in);
		}
	};
}
