package com.alphadevs.hfc.app.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.alphadevs.hfc.app.common.model.FetchDetailsResultDataPojo;
import com.alphadevs.hfc.app.common.model.FieldOfActions;
import com.alphadevs.hfc.app.common.model.SearchRequirements;
import com.alphadevs.hfc.app.core.db.DatabaseHelper;
import com.alphadevs.hfc.app.core.ws.client.WSClient;
import com.alphadevs.hfc.app.serviceconsumers.HfcWSClientConsumer;
import com.alphadevs.hfc.app.util.HFCAndroidSessionManager;
import com.alphadevs.hfc.app.util.HFCConstants;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.dao.Dao;

public class HFCServerCommunicationServices extends Service {

	private WSClient client;
	private final IBinder mBinder = new LocalBinder();
	private static HFCServerCommunicationServices communicationServices;
	public static int updateMetaDataStatus = HFCConstants.UPDATE_NOT_STARTED;
	public class LocalBinder extends Binder {

		public HFCServerCommunicationServices getService() {
			return HFCServerCommunicationServices.this;
		}
	}

	@Override
	public void onDestroy() {
		client = null;
		super.onDestroy();
	};
	// TODO Comment this line.. Meant just for test
	private String loginJsonRes =
			"{  \"status\" : \"SUCCESS\",   \"data\" : {    \"auth_token\" : \"50e7bc2de4b078153882eecd\",    \"user\" : {      \"firstName\" : null,      \"id\" : \"50e7ba3be4b0c259dd99379c\",      \"creationDate\" : 1357363771565,      \"middleName\" : null,      \"email\" : \"karthik001@me.com\",      \"lastLoggedInDate\" : 1357364269259,      \"lastName\" : null    }}}";

	@Override
	public IBinder onBind(Intent arg0) {
		this.client = new WSClient();
		return this.mBinder;
	}

	public void login(final List<NameValuePair> postParams, final Handler handler, final ProgressDialog dialog) {
		new AsyncTask<Void, Void, JSONObject>() {

			@Override
			protected JSONObject doInBackground(Void... params) {
				// TODO Invert the Comments below..
				// return
				// client.getJSONFromUrl("http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/login",postParams);
				try {
					return new JSONObject(loginJsonRes);
				} catch (JSONException e) {
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPreExecute() {
				if (dialog != null)
				;
				dialog.show();
			}

			protected void onPostExecute(JSONObject loginResult) {
				try {
					Message message = new Message();
					Bundle bundle = new Bundle();
					if (loginResult != null) bundle.putString("status", loginResult.getString("status"));
					HFCAndroidSessionManager.startSession(getApplicationContext(), loginResult);
					message.setData(bundle);
					handler.sendMessage(message);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.execute();
	}

	public void getNgoSearchResults(final List<NameValuePair> postParams, final Handler handler, final ProgressDialog dialog) {
		new AsyncTask<Void, Void, JSONObject>() {

			private ProgressDialog dialog;

			@Override
			protected JSONObject doInBackground(Void... params) {
				// client.getJSONFromUrl("http://google.com/login", postParams);
				return null;
			}

			@Override
			protected void onPreExecute() {
				dialog.show(dialog.getContext(), "searchreault", "Loading...");
			}

			protected void onPostExecute(JSONObject result) {
				try {
					Message message = new Message();
					Bundle bundle = new Bundle();
					if (result != null) bundle.putString("status", result.getString("status"));
					message.setData(bundle);
					handler.sendMessage(message);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.execute();
	}
	public static class HFCWServiceConnection implements ServiceConnection {

		private HfcWSClientConsumer consumer;

		public HFCWServiceConnection(HfcWSClientConsumer consumer) {
			this.consumer = consumer;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			consumer.onServiceDisconnected();
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			consumer.onServiceConnected(((LocalBinder) binder).getService());
		}
	};
	private static HFCWServiceConnection serviceCon;

	public static void bindService(Context serviceConsumerActivityConext) {
		// if(serviceCon==null)
		serviceCon = (new HFCWServiceConnection((HfcWSClientConsumer) serviceConsumerActivityConext));
		Intent loginIntent = new Intent(serviceConsumerActivityConext, HFCServerCommunicationServices.class);
		serviceConsumerActivityConext.bindService(loginIntent, serviceCon, Context.BIND_AUTO_CREATE);
	}

	public static void unbindService(Context context) {
		if (serviceCon != null) context.unbindService(serviceCon);
	}

	public void loginWithFB(final List<NameValuePair> postParams, final Handler handler) {
		new AsyncTask<Void, Void, JSONObject>() {

			private ProgressDialog dialog;

			@Override
			protected JSONObject doInBackground(Void... params) {
				// TODO Invert the Comments below..
				/*
				 * return client .getJSONFromUrl(
				 * "http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/loginWithFB"
				 * , postParams);
				 */
				try {
					return new JSONObject(loginJsonRes);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			protected void onPostExecute(JSONObject loginResult) {
				try {
					Message message = new Message();
					Bundle bundle = new Bundle();
					if (loginResult != null) bundle.putString("status", loginResult.getString("status"));
					HFCAndroidSessionManager.startSession(getApplicationContext(), loginResult);
					message.setData(bundle);
					handler.sendMessage(message);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.execute();
	}
	String fetchDetailsResult =
			"{ \"status\" : \"SUCCESS\",\"data\" : { \"requirements\": [{ \"id\": 71051a9fc2e6acf5dcf99c9e,\"name\": \"Edutainment\", \"description\": \"Desc\",\"creationDate\": 1359288991511,\"lastUpdated\": 1359288991511 },{ \"id\": 81051a9fc2e6acf5dcf99c9d,\"name\": \"Education\", \"description\": \"Desc\",\"creationDate\": 1359288991511,\"lastUpdated\": 1359288991511 }], \"fields_of_action\":[{\"id\": 61051a9fc2e6acf5dcf99c91,\"name\": \"Child Education\",\"description\": \"Desc\",\"creationDate\": 1359288991511,\"lastUpdated\": 1359288991511 },{\"id\": 61051a9fc2e6acf5dcf99c92,\"name\": \"Education\",\"description\": \"Desc\",\"creationDate\": 1359288991511,\"lastUpdated\": 1359288991511 }]}}";

	public void updateAppMetaData(final DatabaseHelper dbHelper, final Handler handler) {
		new AsyncTask<Void, Void, JSONObject>() {

			private static final String sessionSharedPrefName = "LatUpdateSharePref";
			private SharedPreferences lastUpadetTSSharedPref;

			@Override
			protected JSONObject doInBackground(Void... params) {
				updateMetaDataStatus = HFCConstants.UPDATE_IN_PROGRESS;
				lastUpadetTSSharedPref = getApplicationContext().getSharedPreferences(sessionSharedPrefName, MODE_PRIVATE);
				Long lastUpdateTimeInMilliSec = lastUpadetTSSharedPref.getLong("LatUpdatedTimeInMilliSec", 0);
				// TODO Invert the Comments below..
				/*
				 * set params as the lat updated timestamp from shared repos;
				 * 
				 * return client .getJSONFromUrl(
				 * "http://ec2-54-234-87-194.compute-1.amazonaws.com:8080/rest-api-1.0/fetchDetaisl?timeStamp="
				 * +(new Timestamp(lastUpdateTimeInMilliSec), postParams);
				 */
				try {
					return new JSONObject(fetchDetailsResult);
				} catch (JSONException e) {
					updateMetaDataStatus = HFCConstants.UPDATE_FAILED;
					e.printStackTrace();
					return null;
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			protected void onPostExecute(JSONObject fetchDetailsResult) {
				try {
					Message message = new Message();
					Bundle bundle = new Bundle();
					if (fetchDetailsResult != null) bundle.putString("status", fetchDetailsResult.getString("status"));
					FetchDetailsResultDataPojo resultPojo = (new ObjectMapper()).readValue(fetchDetailsResult.getString("data"), FetchDetailsResultDataPojo.class);
					Dao<SearchRequirements, Integer> searchRequirementsDAO = dbHelper.getDao(SearchRequirements.class);
					Dao<FieldOfActions, Integer> fieldOfActionsDAO = dbHelper.getDao(FieldOfActions.class);
					for (int i = 0; i < resultPojo.getSearchRequirements().length || i < resultPojo.getFieldOfActions().length; i++) {
						try {
							if (i < resultPojo.getSearchRequirements().length) {
								searchRequirementsDAO.createOrUpdate(resultPojo.getSearchRequirements()[i]);
							}
							if (i < resultPojo.getFieldOfActions().length) {
								fieldOfActionsDAO.createOrUpdate(resultPojo.getFieldOfActions()[i]);
							}
							lastUpadetTSSharedPref.edit().putLong("LatUpdatedTimeInMilliSec", System.currentTimeMillis());
							lastUpadetTSSharedPref.edit().commit();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
					updateMetaDataStatus = HFCConstants.UPDATE_COMPLETE;
					message.setData(bundle);
					handler.sendMessage(message);
				} catch (JSONException e) {
					updateMetaDataStatus = HFCConstants.UPDATE_FAILED;
					e.printStackTrace();
				} catch (JsonParseException e) {
					updateMetaDataStatus = HFCConstants.UPDATE_FAILED;
					e.printStackTrace();
				} catch (JsonMappingException e) {
					updateMetaDataStatus = HFCConstants.UPDATE_FAILED;
					e.printStackTrace();
				} catch (IOException e) {
					updateMetaDataStatus = HFCConstants.UPDATE_FAILED;
					e.printStackTrace();
				}
			}
		}.execute();
	}
}
